#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D u_texture;
uniform sampler2D u_texture2;

uniform float offset;

varying vec4 v_color;
varying vec2 v_texCoords;

void main() {

	vec4 backgroundColor = v_color * texture2D(u_texture, v_texCoords);	
	vec4 mountainColor =  v_color * texture2D(u_texture2,v_texCoords-vec2(offset,0.0));
	
	gl_FragColor = backgroundColor*(1.0-mountainColor.a)+mountainColor*mountainColor.a;	
}