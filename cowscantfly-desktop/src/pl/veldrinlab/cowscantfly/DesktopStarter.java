package pl.veldrinlab.cowscantfly;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Class represents Desktop Starter which is entry point of application on PC. It is used to configure and launch game. 
 * @author Szymon Jab�o�ski
 *
 */
class DesktopStarter {
	/**
	 * Entry point of desktop application
	 * @param args is main arguments.
	 */
	public static void main(String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Configuration.getInstance().width;
		config.height = Configuration.getInstance().height;
		config.title = Configuration.getInstance().windowTitle;
		config.useGL20 = Configuration.getInstance().useGL20;
		config.fullscreen = Configuration.getInstance().fullScreenEnabled;
		new LwjglApplication(new CowsCantFly(), config);
	}
}
