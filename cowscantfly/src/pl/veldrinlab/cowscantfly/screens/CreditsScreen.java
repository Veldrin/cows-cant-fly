package pl.veldrinlab.cowscantfly.screens;

import pl.veldrinlab.cowscantfly.Configuration;
import pl.veldrinlab.cowscantfly.CowsCantFly;
import pl.veldrinlab.sakuraEngine.core.GameScreen;
import pl.veldrinlab.sakuraEngine.core.Renderer;
import pl.veldrinlab.sakuraEngine.core.SpriteActor;

import pl.veldrinlab.sakuraEngine.core.Timer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * Class represents credits screen. Show information about game authors. It is part of Menu state.
 * @author Szymon Jab�o�ski
 *
 */
public class CreditsScreen extends GameScreen implements GestureListener  {
	
	public MenuScreen menuScreen;
	
	private CowsCantFly game;
	private GestureDetector inputDetector;
	
	private SpriteActor backToMenuground;
	
	private Label authors;
	private Label jablonski;
	private Label korniluk;
	private Label swiniarski;
	private Label jablonska;
	private Label backToMenu;

	private float blinking;
	
	public CreditsScreen(final CowsCantFly game) {
		this.game = game;
	
		backToMenuground = new SpriteActor(game.resources.getTexture("menuBackground"));   
		
    	LabelStyle style = new LabelStyle(Renderer.defaultFont,Color.WHITE);
    	
    	authors = new Label("Authors",style);
    	jablonski = new Label("Szymon Jablonski - engine code",style);
    	korniluk = new Label("Marcin Korniluk - gameplay code",style);
    	swiniarski = new Label("Karol Swiniarski - gameplay code",style);
    	jablonska = new Label("Malgorzata Jablonska - graphics",style);
    	backToMenu = new Label("Back to menu",style);

    	inputDetector = new GestureDetector(this);    	
    	initializeInterface();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
		Renderer.defaultStage.clear();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void render(float deltaTime) {
		processInput();
		game.timer.updateTimer(deltaTime);
		while(game.timer.checkTimerAccumulator()) {
			processLogic(Timer.TIME_STEP);
			game.timer.eatAccumulatorTime();
		}
		processRendering();	
	}

	@Override
	public void resize(final int width, final int height) {
		Renderer.defaultStage.setViewport(Configuration.getInstance().width, Configuration.getInstance().height, false);
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void show() {
		Renderer.defaultStage.clear();
    	Renderer.defaultStage.addActor(backToMenuground);
    	Renderer.defaultStage.addActor(authors);
    	Renderer.defaultStage.addActor(jablonski);
    	Renderer.defaultStage.addActor(korniluk);
    	Renderer.defaultStage.addActor(swiniarski);
    	Renderer.defaultStage.addActor(jablonska);
    	Renderer.defaultStage.addActor(backToMenu);
    	Gdx.input.setInputProcessor(inputDetector); 	
	}

	@Override
	public void processInput() {
	}

	@Override
	public void processLogic(final float deltaTime) {
		
		blinking += deltaTime*5.0f;	
		backToMenu.setColor(1.0f, 1.0f, 1.0f, (float) ((Math.sin(blinking)+1.0f)/2.0f));
	}

	@Override
	public void processRendering() {
		Renderer.clearScreen();
		Renderer.defaultStage.draw();
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		return false;
	}


	@Override
	public boolean zoom(float arg0, float arg1) {
		return false;
	}
	
	private void initializeInterface() {
		authors.setTouchable(Touchable.disabled);
		jablonski.setTouchable(Touchable.disabled);
		korniluk.setTouchable(Touchable.disabled);
		swiniarski.setTouchable(Touchable.disabled);
		jablonska.setTouchable(Touchable.disabled);
		
		backToMenu.setName("Back");
		
		authors.setX((Configuration.getInstance().width-authors.getTextBounds().width)*0.5f);	
		authors.setY(Configuration.getInstance().height*0.90f - authors.getTextBounds().height);
		jablonski.setX((Configuration.getInstance().width-jablonski.getTextBounds().width)*0.5f);	
		jablonski.setY(Configuration.getInstance().height*0.75f - jablonski.getTextBounds().height);
		korniluk.setX((Configuration.getInstance().width-korniluk.getTextBounds().width)*0.5f);	
		korniluk.setY(Configuration.getInstance().height*0.65f - korniluk.getTextBounds().height);
		swiniarski.setX((Configuration.getInstance().width-swiniarski.getTextBounds().width)*0.5f);	
		swiniarski.setY(Configuration.getInstance().height*0.55f - swiniarski.getTextBounds().height);
		jablonska.setX((Configuration.getInstance().width-jablonska.getTextBounds().width)*0.5f);	
		jablonska.setY(Configuration.getInstance().height*0.45f - jablonska.getTextBounds().height);
		backToMenu.setX((Configuration.getInstance().width-backToMenu.getTextBounds().width)*0.5f);	
		backToMenu.setY(Configuration.getInstance().height*0.30f - backToMenu.getTextBounds().height);
	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int arg2, int arg3) {
		Vector2 stageCoords = Vector2.tmp;
		Renderer.defaultStage.screenToStageCoordinates(stageCoords.set(Gdx.input.getX(), Gdx.input.getY()));
		Actor actor = Renderer.defaultStage.hit(stageCoords.x, stageCoords.y, true);
		
		if(actor == null)
			return false;
		
		if(actor.getName().equals("Back")) {
			game.setScreen(menuScreen);
		}
		
		return true;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}
}
