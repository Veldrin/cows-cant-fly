package pl.veldrinlab.cowscantfly.screens;

import pl.veldrinlab.cowscantfly.Animation;
import pl.veldrinlab.cowscantfly.Configuration;
import pl.veldrinlab.cowscantfly.Cow;
import pl.veldrinlab.cowscantfly.CowsCantFly;
import pl.veldrinlab.sakuraEngine.core.GameScreen;
import pl.veldrinlab.sakuraEngine.core.Renderer;
import pl.veldrinlab.sakuraEngine.core.SpriteActor;

import pl.veldrinlab.sakuraEngine.core.Timer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * Class represents credits screen. Show information about game instruction. It is part of Menu state.
 * @author Szymon Jab�o�ski
 *
 */
public class InstructionScreen extends GameScreen implements GestureListener  {

	public MenuScreen menuScreen;
	
	private CowsCantFly game;
	private GestureDetector inputDetector;
	
	private SpriteActor backToMenuground;
	
	private Cow jumpCow;
	private Cow attackCow;
	
	private Label instruction;
	private Label jumpInstruction;
	private Label attackInstruction;
	private Label mainGoal;
	private Label goalDescription;
	private Label backToMenu;

	private float blinking;
	
	public InstructionScreen(final CowsCantFly game) {
		this.game = game;
	
		backToMenuground = new SpriteActor(game.resources.getTexture("menuBackground"));  
		
		jumpCow = new Cow(game.resources.getTexture("cowFly01"));
		attackCow = new Cow(game.resources.getTexture("cowAttack01"));
		
    	LabelStyle style = new LabelStyle(Renderer.defaultFont,Color.WHITE);
    	
    	instruction = new Label("Instruction",style);
    	jumpInstruction = new Label("Tap screen to jump!",style);
    	attackInstruction = new Label("Swipe left to right to attack!",style);   
    	mainGoal = new Label("Don't stop dreaming!",style);
    	goalDescription = new Label("Destroy alarms, collect cow bells",style);	
    	backToMenu = new Label("Back to menu",style);
    	
    	inputDetector = new GestureDetector(this);    	
    	
    	initCows();
    	initializeInterface();
	}

	private void initCows() {	
		jumpCow.setPosition(50.0f, 350.0f);
		attackCow.setPosition(550.0f, 350.0f);

		Animation flyAnimation = new Animation();
		
		flyAnimation.frames.add(game.resources.getTexture("cowFly01"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly02"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly03"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly04"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly05"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly06"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly07"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly08"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly09"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly10"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly11"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly12"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly13"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly14"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly15"));
		flyAnimation.frames.add(game.resources.getTexture("cowFly16"));
		jumpCow.animations.add(flyAnimation);
		
		Animation attackAnimation = new Animation();
		
		attackAnimation.frames.add(game.resources.getTexture("cowAttack01"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack02"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack03"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack04"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack05"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack06"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack07"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack08"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack09"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack10"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack11"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack12"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack13"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack14"));
		attackAnimation.frames.add(game.resources.getTexture("cowAttack15"));
		attackCow.animations.add(attackAnimation);
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
		Renderer.defaultStage.clear();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void render(float deltaTime) {
		processInput();
		game.timer.updateTimer(deltaTime);
		while(game.timer.checkTimerAccumulator()) {
			processLogic(Timer.TIME_STEP);
			game.timer.eatAccumulatorTime();
		}
		processRendering();	
	}

	@Override
	public void resize(final int width, final int height) {
		Renderer.defaultStage.setViewport(Configuration.getInstance().width, Configuration.getInstance().height, false);
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void show() {
		Renderer.defaultStage.clear();

    	Renderer.defaultStage.addActor(backToMenuground);
    	Renderer.defaultStage.addActor(instruction);
    	Renderer.defaultStage.addActor(jumpCow);
    	Renderer.defaultStage.addActor(attackCow);
    	Renderer.defaultStage.addActor(jumpInstruction);
    	Renderer.defaultStage.addActor(attackInstruction);
    	Renderer.defaultStage.addActor(mainGoal);
    	Renderer.defaultStage.addActor(goalDescription);
    	Renderer.defaultStage.addActor(backToMenu);
    	
    	Gdx.input.setInputProcessor(inputDetector); 	
	}

	@Override
	public void processInput() {
	}

	@Override
	public void processLogic(final float deltaTime) {
		
		//TODO maybe some method for this, it's very ugly
		jumpCow.getSprite().setTexture(jumpCow.animations.get(0).update(deltaTime));
		attackCow.getSprite().setTexture(attackCow.animations.get(0).update(deltaTime));
		
		blinking += deltaTime*5.0f;
		
		float alpha = (float) ((Math.sin(blinking)+1.0f)/2.0f);
		
		backToMenu.setColor(1.0f, 1.0f, 1.0f, alpha);
	}

	@Override
	public void processRendering() {
		Renderer.clearScreen();
		Renderer.defaultStage.draw();
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		return false;
	}


	@Override
	public boolean zoom(float arg0, float arg1) {
		return false;
	}
	
	private void initializeInterface() {
		instruction.setTouchable(Touchable.disabled);
		jumpInstruction.setTouchable(Touchable.disabled);
		attackInstruction.setTouchable(Touchable.disabled);
		mainGoal.setTouchable(Touchable.disabled);
		goalDescription.setTouchable(Touchable.disabled);
		backToMenu.setName("Back");
		
		instruction.setX((Configuration.getInstance().width-instruction.getTextBounds().width)*0.5f);	
		instruction.setY(Configuration.getInstance().height*0.90f - instruction.getTextBounds().height);
	
		jumpInstruction.setX((Configuration.getInstance().width-jumpInstruction.getTextBounds().width)*0.5f);	
		jumpInstruction.setY(Configuration.getInstance().height*0.60f - jumpInstruction.getTextBounds().height);
	
		attackInstruction.setX((Configuration.getInstance().width-attackInstruction.getTextBounds().width)*0.5f);	
		attackInstruction.setY(Configuration.getInstance().height*0.50f - attackInstruction.getTextBounds().height);
	
		
		
		mainGoal.setX((Configuration.getInstance().width-mainGoal.getTextBounds().width)*0.5f);	
		mainGoal.setY(Configuration.getInstance().height*0.40f - mainGoal.getTextBounds().height);
		
		goalDescription.setX((Configuration.getInstance().width-goalDescription.getTextBounds().width)*0.5f);	
		goalDescription.setY(Configuration.getInstance().height*0.30f - goalDescription.getTextBounds().height);
		
		backToMenu.setX((Configuration.getInstance().width-backToMenu.getTextBounds().width)*0.5f);	
		backToMenu.setY(Configuration.getInstance().height*0.10f - backToMenu.getTextBounds().height);
	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int arg2, int arg3) {
		Vector2 stageCoords = Vector2.tmp;
		Renderer.defaultStage.screenToStageCoordinates(stageCoords.set(Gdx.input.getX(), Gdx.input.getY()));
		Actor actor = Renderer.defaultStage.hit(stageCoords.x, stageCoords.y, true);
		
		if(actor == null)
			return false;
		
		if(actor.getName().equals("Back")) {
			game.setScreen(menuScreen);
		}
		
		return true;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}
}
