package pl.veldrinlab.cowscantfly.screens;

import pl.veldrinlab.cowscantfly.AnimatedEntity;
import pl.veldrinlab.cowscantfly.Animation;
import pl.veldrinlab.cowscantfly.Configuration;
import pl.veldrinlab.cowscantfly.CowsCantFly;
import pl.veldrinlab.sakuraEngine.core.GameScreen;
import pl.veldrinlab.sakuraEngine.core.Renderer;
import pl.veldrinlab.sakuraEngine.core.SpriteActor;
import pl.veldrinlab.sakuraEngine.core.Timer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Class represents Loading screen. Show game logo and load load all resources defined in reosurces descriptor file. It is part of Intro state.
 * @author Szymon Jab�o�ski
 *
 */
public class MenuScreen extends GameScreen implements GestureListener {

	public PlayScreen playScreen;
	public CreditsScreen informationScreen;
	public SettingsScreen settingsScreen;
	public InstructionScreen instructionScreen;
	
	private CowsCantFly game;
	private GestureDetector inputDetector;

	private AnimatedEntity background;
	private SpriteActor cloud0;
	private SpriteActor cloud1;
	private SpriteActor cloud2;
	private SpriteActor play;
	private SpriteActor exit;
	private SpriteActor skipAnimation;
	private SpriteActor settings;
	private SpriteActor instruction;
	private SpriteActor informations;

	private float blendAccumulator;
	private float blinking;
	private float cloudBlend;
	private float cloudBlendingVelocity;
	
	private Music menuMusic;

	public MenuScreen(final CowsCantFly game) {
		this.game = game;

		Animation backgroundAnimation = new Animation();
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground0"));
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground1"));
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground2"));
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground3"));
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground4"));
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground3"));
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground2"));
		backgroundAnimation.frames.add(game.resources.getTexture("loadingBackground1"));

		background = new AnimatedEntity(game.resources.getTexture("loadingBackground0"));
		background.animations.add(backgroundAnimation); //TODO encapsulate

		cloud0 = new SpriteActor(game.resources.getTexture("splashCloud0"));
		cloud1 = new SpriteActor(game.resources.getTexture("splashCloud1"));
		cloud2 = new SpriteActor(game.resources.getTexture("splashCloud2"));

		play = new SpriteActor(game.resources.getTexture("play"),"Play");
		exit = new SpriteActor(game.resources.getTexture("exit"),"Exit");
		settings = new SpriteActor(game.resources.getTexture("settings"),"Settings");
		instruction = new SpriteActor(game.resources.getTexture("instruction"),"Instruction");
		informations = new SpriteActor(game.resources.getTexture("informations"),"Informations");
		skipAnimation = new SpriteActor(game.resources.getTexture("skip"),"Skip");

		menuMusic = game.resources.getMusic("menuMusic");
		
		inputDetector = new GestureDetector(this);

		initializeInterface();
	}

	public void init() {
		blendAccumulator = 1.0f;
		blinking = 0.0f;
		cloudBlendingVelocity = 0.0f;
		cloudBlend = 0.0f;
		
		play.setVisible(true);
		exit.setVisible(true);
		informations.setVisible(true);
		instruction.setVisible(true);
		settings.setVisible(true);
		skipAnimation.setVisible(false);
	}
	
	@Override
	public void processInput() {
	}

	@Override
	public void processLogic(final float deltaTime) {

		background.update(deltaTime*0.25f);
		blinking += deltaTime*5.0f;
		float alpha = (float) ((Math.sin(blinking)+1.0f)/2.0f);

		blendAccumulator += deltaTime*0.40f;
		blendAccumulator = MathUtils.clamp(blendAccumulator, 0.0f, 1.0f);

		background.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		informations.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		settings.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		instruction.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);

		if(blendAccumulator < 1.0f) {
			play.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
			exit.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		}
		else {
			play.getSprite().setColor(1.0f, 1.0f, 1.0f, alpha);
			exit.getSprite().setColor(1.0f, 1.0f, 1.0f, alpha);
		}
		
		skipAnimation.getSprite().setColor(1.0f, 1.0f, 1.0f, alpha);
		cloudBlend+=deltaTime*cloudBlendingVelocity;

		cloud0.getSprite().setColor(1.0f, 1.0f, 1.0f, Math.max(Math.min(cloudBlend*2,1.0f), 0.0f));
		cloud1.getSprite().setColor(1.0f, 1.0f, 1.0f, Math.max(Math.min(cloudBlend*2-0.5f,1.0f), 0.0f));			
		cloud2.getSprite().setColor(1.0f, 1.0f, 1.0f, Math.max(Math.min(cloudBlend*2-1.0f,1.0f), 0.0f));

		float positionY = -(Math.max(Math.min(cloudBlend-1.0f,2.0f), 0.0f))*Configuration.getInstance().height*0.5f;

		background.getSprite().setPosition(0.0f, positionY);
		cloud0.getSprite().setPosition(0.0f, positionY);
		cloud1.getSprite().setPosition(0.0f, positionY);
		cloud2.getSprite().setPosition(0.0f, positionY);

		if(cloudBlend > 3.0f)
			executeGame();
	}

	@Override
	public void processRendering() {	
		Renderer.clearScreen(Color.WHITE);
		Renderer.defaultStage.draw();
	}

	@Override
	public void render(final float deltaTime) {
		if(deltaTime > 0.5f)
			return;

		processInput();
		game.timer.updateTimer(deltaTime);
		while(game.timer.checkTimerAccumulator()) {
			processLogic(Timer.TIME_STEP);
			game.timer.eatAccumulatorTime();
		}
		processRendering();		
	}

	@Override
	public void resize(final int width, final int height) {
		Renderer.defaultStage.setViewport(Configuration.getInstance().width, Configuration.getInstance().height, false);	
	}

	@Override
	public void hide() {
		Renderer.defaultStage.clear();
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		//	 TODO Auto-generated method stub
	}

	@Override
	public void show() {	
		
		if(Configuration.getInstance().musicOn)
			menuMusic.play();

		Renderer.defaultStage.addActor(background);
		Renderer.defaultStage.addActor(play);
		Renderer.defaultStage.addActor(exit);
		Renderer.defaultStage.addActor(cloud0);
		Renderer.defaultStage.addActor(cloud1);
		Renderer.defaultStage.addActor(cloud2);
		Renderer.defaultStage.addActor(skipAnimation);
		Renderer.defaultStage.addActor(settings);
		Renderer.defaultStage.addActor(informations);
		Renderer.defaultStage.addActor(instruction);

		background.getSprite().setColor(1.0f,1.0f,1.0f,blendAccumulator);
		cloud0.getSprite().setColor(1.0f,1.0f,1.0f,blendAccumulator);
		cloud1.getSprite().setColor(1.0f,1.0f,1.0f,blendAccumulator);
		cloud2.getSprite().setColor(1.0f,1.0f,1.0f,blendAccumulator);
		play.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		exit.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		informations.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		settings.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);
		instruction.getSprite().setColor(1.0f, 1.0f, 1.0f, blendAccumulator);

		skipAnimation.setVisible(false);
		Gdx.input.setInputProcessor(inputDetector);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		return false;
	}

	@Override
	public boolean zoom(float arg0, float arg1) {
		return false;
	}

	private void initializeInterface() {
		play.getSprite().setPosition(50.0f, 80.0f);
		exit.getSprite().setPosition(50.0f, 24.0f);
		skipAnimation.getSprite().setPosition(50.0f, 80.0f);

		settings.getSprite().setPosition(Configuration.getInstance().width-settings.getSprite().getWidth(), 0.0f);
		informations.getSprite().setPosition(Configuration.getInstance().width-informations.getSprite().getWidth()*2.0f, 0.0f);
		instruction.getSprite().setPosition(Configuration.getInstance().width-instruction.getSprite().getWidth(), instruction.getSprite().getHeight());

		informations.setBounds(informations.getSprite().getX(), informations.getSprite().getY(), informations.getSprite().getWidth(), informations.getSprite().getHeight());
		settings.setBounds(settings.getSprite().getX(), settings.getSprite().getY(), settings.getSprite().getWidth(), settings.getSprite().getHeight());
		instruction.setBounds(instruction.getSprite().getX(), instruction.getSprite().getY(), instruction.getSprite().getWidth(), instruction.getSprite().getHeight());
		play.setBounds(play.getSprite().getX(), play.getSprite().getY(), play.getSprite().getWidth(), play.getSprite().getHeight());
		exit.setBounds(exit.getSprite().getX(), exit.getSprite().getY(), exit.getSprite().getWidth(), exit.getSprite().getHeight());
		skipAnimation.setBounds(skipAnimation.getSprite().getX(), skipAnimation.getSprite().getY(), skipAnimation.getSprite().getWidth(), skipAnimation.getSprite().getHeight());
	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float arg0, float arg1, int arg2, int arg3) {	
		Vector2 stageCoords = Vector2.tmp;
		Renderer.defaultStage.screenToStageCoordinates(stageCoords.set(Gdx.input.getX(), Gdx.input.getY()));
		Actor actor = Renderer.defaultStage.hit(stageCoords.x, stageCoords.y, true);

		if(actor == null)
			return false;

		if(actor.getName().equals("Informations")) {
			game.setScreen(informationScreen);
			blendAccumulator = 1.0f;
			return true;
		}
		else if(actor.getName().equals("Settings")) {
			game.setScreen(settingsScreen);
			blendAccumulator = 1.0f;
			return true;
		}
		else if(actor.getName().equals("Instruction")) {
			game.setScreen(instructionScreen);
			blendAccumulator = 1.0f;
			return true;
		}
		else if(actor.getName().equals("Skip")) {
			blendAccumulator = 1.0f;
			executeGame();
			return true;
		}
		else if(actor.getName().equals("Play")) {
			blendAccumulator = 1.0f;
			cloudBlendingVelocity = 0.25f;
			play.setVisible(false);
			exit.setVisible(false);
			informations.setVisible(false);
			instruction.setVisible(false);
			settings.setVisible(false);
			skipAnimation.setVisible(true);
			return true;			
		}
		else if(actor.getName().equals("Exit"))
			Gdx.app.exit();

		return false;
	}

	private void executeGame() {
		playScreen.init();
		game.setScreen(playScreen);
		if(Configuration.getInstance().musicOn)
			menuMusic.stop();
		dispose();
	}
	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}
}
