package pl.veldrinlab.cowscantfly.screens;

import pl.veldrinlab.cowscantfly.Configuration;
import pl.veldrinlab.cowscantfly.CowsCantFly;
import pl.veldrinlab.sakuraEngine.core.GameScreen;
import pl.veldrinlab.sakuraEngine.core.Renderer;
import pl.veldrinlab.sakuraEngine.core.SpriteActor;
import pl.veldrinlab.sakuraEngine.core.Timer;
import pl.veldrinlab.sakuraEngine.fx.FadeEffectParameters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

/**
 * Class represents Splash screen. Show game/engine/team logo. It is part of Intro state.
 * @author Szymon Jab�o�ski
 *
 */
public class SplashScreen extends GameScreen {

	public GameScreen nextScreen;
	
	private CowsCantFly game;
	private SpriteActor splash;
	private FadeEffectParameters fade;
	
	private float effectTime;
	private float elapsedTime;

	//TODO next screen int
    public SplashScreen(final CowsCantFly game, final FadeEffectParameters fadeParams) {
    	this.game = game;
    	this.fade = fadeParams;
    	splash = new SpriteActor(game.resources.getTexture(fadeParams.textureName));
    }
    
	@Override
	public void processInput() {		

	}

	@Override
	public void processLogic(final float deltaTime) {
			
		if((fade.skippable && fade.skippableWhileFadingIn && Gdx.input.justTouched()) || elapsedTime > effectTime) {
			game.setScreen(nextScreen);
			dispose();
		}
		
		elapsedTime += deltaTime;
		
		if(elapsedTime < fade.fadeInTime)
			splash.getSprite().setColor(1.0f, 1.0f, 1.0f, elapsedTime);
		else if (elapsedTime < fade.fadeInTime + fade.stayTime)
			splash.getSprite().setColor(1.0f, 1.0f, 1.0f, 1.0f);
		else if(elapsedTime < fade.fadeInTime + fade.stayTime + fade.fadeOutTime)
			splash.getSprite().setColor(1.0f, 1.0f, 1.0f, effectTime-elapsedTime);
	}

	@Override
	public void processRendering() {
		Renderer.clearScreen(Color.BLACK);
		Renderer.defaultStage.draw();	
	}
    
	@Override
	public void render(final float deltaTime) {
		if(deltaTime > 0.5f)
			return;
		
		processInput();
		game.timer.updateTimer(deltaTime);
		while(game.timer.checkTimerAccumulator()) {
			processLogic(Timer.TIME_STEP);
			game.timer.eatAccumulatorTime();
		}
		processRendering();
	}

	@Override
	public void resize(int width, int height) {
		Renderer.defaultStage.setViewport(Configuration.getInstance().width,Configuration.getInstance().height, false);
	}
	
	@Override
	public void hide() {
		Renderer.defaultStage.clear();
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void show() {
		effectTime = fade.fadeInTime + fade.stayTime + fade.fadeOutTime;
		elapsedTime = 0.0f;
		
		splash.getSprite().setColor(1.0f, 1.0f, 1.0f, elapsedTime);
		
		Renderer.defaultStage.addActor(splash);
    	Gdx.input.setInputProcessor(Renderer.defaultStage);
	}

	@Override
	public void dispose() {

	}
}
