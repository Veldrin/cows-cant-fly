package pl.veldrinlab.cowscantfly.screens;

import pl.veldrinlab.cowscantfly.Configuration;
import pl.veldrinlab.cowscantfly.CowsCantFly;
import pl.veldrinlab.sakuraEngine.core.GameScreen;
import pl.veldrinlab.sakuraEngine.core.Renderer;
import pl.veldrinlab.sakuraEngine.core.SpriteActor;
import pl.veldrinlab.sakuraEngine.core.Timer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * Class represents Pause screen. From them user can back to game, exit the game or recalibrate device default orientation before play again. It is part of Play state.
 * @author Szymon Jab�o�ski
 *
 */
public class PauseScreen extends GameScreen implements GestureListener  {
	
	public PlayScreen playScreen;
	
	private CowsCantFly game;
	private GestureDetector inputDetector;

	private FrameBuffer renderTarget;
	private Stage pauseStage;
	private SpriteBatch pauseBatch;
	private SpriteActor pauseBackground;
	
	private Label pause;
	private Label resume;
	private Label exit;
	private float blinking;
	private float alpha;
	
	public PauseScreen(final CowsCantFly game) {
		this.game = game;
		
		LabelStyle style = new LabelStyle(Renderer.defaultFont,Color.WHITE);
		
		pauseBatch = new SpriteBatch();
		renderTarget = new FrameBuffer(Pixmap.Format.RGBA8888,Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),true);
		pauseStage = new Stage(Configuration.getInstance().width, Configuration.getInstance().height, false, pauseBatch);
		pauseBackground = new SpriteActor(renderTarget.getColorBufferTexture());
		pauseBackground.getSprite().flip(false,true);
		
		pause = new Label("Pause",style);
		resume = new Label("Resume",style);
		exit = new Label("Exit",style);
		
		inputDetector = new GestureDetector(this);
		
		alpha = 0.4f;
		initializeInterface();
	}
	
	public FrameBuffer getFrameBuffer() {
		return renderTarget;
	}
	
	@Override
	public void dispose() {
		pauseBatch.dispose();
		pauseStage.dispose();
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
		pauseStage.clear();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void render(final float deltaTime) {
		processInput();
		game.timer.updateTimer(deltaTime);
		while(game.timer.checkTimerAccumulator()) {
			processLogic(Timer.TIME_STEP);
			game.timer.eatAccumulatorTime();
		}
		processRendering();	
	}

	@Override
	public void resize(final int width, final int height) {
 		Renderer.defaultStage.setViewport(Configuration.getInstance().width, Configuration.getInstance().height, false);		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void show() {		
		pauseStage.addActor(pauseBackground);
		pauseStage.addActor(pause);
		pauseStage.addActor(resume);
		pauseStage.addActor(exit);
		
    	Gdx.input.setInputProcessor(inputDetector);
	}

	@Override
	public void processInput() {
	}

	@Override
	public void processLogic(final float deltaTime) {
		blinking += deltaTime*5.0f;	
		float alpha = (float) ((Math.sin(blinking)+1.0f)/2.0f);	
		resume.setColor(1.0f, 1.0f, 1.0f, alpha);
		exit.setColor(1.0f, 1.0f, 1.0f, alpha);
	}

	@Override
	public void processRendering() {
		Renderer.clearScreen();
		pauseBackground.getSprite().setTexture(renderTarget.getColorBufferTexture());
		pauseBackground.getSprite().setColor(1.0f,1.0f,1.0f,alpha);
		pauseStage.draw();
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		return false;
	}

	@Override
	public boolean zoom(float arg0, float arg1) {
		return false;
	}
	
	private void initializeInterface() {
		pause.setTouchable(Touchable.disabled);
		
		resume.setName("Resume");
		exit.setName("Exit");
		
		pause.setX((Configuration.getInstance().width-pause.getTextBounds().width)*0.5f);	
		pause.setY(Configuration.getInstance().height*0.75f - pause.getTextBounds().height);
		
		resume.setX((Configuration.getInstance().width-resume.getTextBounds().width)*0.5f);	
		resume.setY(Configuration.getInstance().height*0.50f - resume.getTextBounds().height);
		exit.setX((Configuration.getInstance().width-exit.getTextBounds().width)*0.5f);	
		exit.setY(Configuration.getInstance().height*0.40f - exit.getTextBounds().height);
	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int arg2, int arg3) {
		Vector2 stageCoords = Vector2.tmp;
		pauseStage.screenToStageCoordinates(stageCoords.set(Gdx.input.getX(), Gdx.input.getY()));
		Actor actor = pauseStage.hit(stageCoords.x, stageCoords.y, true);

		if(actor == null)
			return false;

		if(actor.getName().equals("Resume"))
			game.setScreen(playScreen);

		else if(actor.getName().equals("Exit"))
			Gdx.app.exit();

		return true;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}
}
