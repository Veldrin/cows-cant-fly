package pl.veldrinlab.cowscantfly;

public class Configuration{ 
	
	// Application Options
	
	public int width;
	public int height;
	public boolean useGL20;
	public boolean fullScreenEnabled;
	public String windowTitle;
	public String resourcePath;
	public String highscoreSavePath;
	
	// Game Options
	
	public boolean soundOn;
	public boolean musicOn;
	public HighScoreDescriptor highscoreDescriptor;
	public int score;

	private Configuration() {
		width = 800;
		height = 480;
		useGL20 = true;
		fullScreenEnabled = false;
		windowTitle = "Cows can't fly";
		resourcePath = "resources.json";
		highscoreSavePath = "highscore.json";
		
		highscoreDescriptor = new HighScoreDescriptor();
		musicOn = soundOn = false;
		score = 0;
	}

	private static class ConfigurationHolder { 
		private static final Configuration instance = new Configuration();
	}

	public static Configuration getInstance() {
		return ConfigurationHolder.instance;
	}
}
