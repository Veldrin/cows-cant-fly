package pl.veldrinlab.cowscantfly;

import pl.veldrinlab.cowscantfly.Configuration;
import pl.veldrinlab.cowscantfly.screens.CreditsScreen;
import pl.veldrinlab.cowscantfly.screens.InstructionScreen;
import pl.veldrinlab.cowscantfly.screens.GameOverScreen;
import pl.veldrinlab.cowscantfly.screens.MenuScreen;
import pl.veldrinlab.cowscantfly.screens.PlayScreen;
import pl.veldrinlab.cowscantfly.screens.SettingsScreen;
import pl.veldrinlab.cowscantfly.screens.PauseScreen;
import pl.veldrinlab.cowscantfly.screens.SplashScreen;

import pl.veldrinlab.sakuraEngine.core.AsyncResourceManager;
import pl.veldrinlab.sakuraEngine.core.Renderer;
import pl.veldrinlab.sakuraEngine.core.Timer;
import pl.veldrinlab.sakuraEngine.fx.FadeEffectParameters;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.Json;

/**
 * Class represents game main class. Store game states ans engine, intialize them and starts game.
 * Class extends libgdx Game class which is used by Android Activity.
 * @author Szymon Jab�o�ski
 *
 */
public class CowsCantFly extends Game {
	
	public Timer timer;
	public AsyncResourceManager resources;
	
	public SplashScreen teamSplashScreen;
	public SplashScreen engineSplashScreen;
	public MenuScreen menuScreen;
	public SettingsScreen settingsScreen;
	public CreditsScreen creditsScreen;
	public InstructionScreen instructionScreen;
	public PlayScreen playScreen;
	public PauseScreen pauseScreen;
	public GameOverScreen gameOverScreen;
	
	/**
	 * Class construtor, nothing to do (it is great!).
	 */
	public CowsCantFly() {
		
	}
	
	/**
	 * Method is used to create game data. It it executed by Activity. Initialize game data and setup first screen - spin that shit!
	 */
	@Override
	public void create() {
		timer = new Timer();
		resources = new AsyncResourceManager();
		
		initializeEngine();
		loadHighScore();
		initializeGame();
	}

	private void initializeEngine() {
		resources.loadResources(Configuration.getInstance().resourcePath);
		resources.finishLoading();
		Renderer.defaultFont = resources.getFont("defaultFont");
		//TODO small Renderer
//		Renderer.defaultFontSmall = resources.getFont("defaultFontSmall");
	}
		
	public void initializeGame() {
		FadeEffectParameters fadeEffect = new FadeEffectParameters();
		
		fadeEffect.fadeInTime = 1.0f;
		fadeEffect.stayTime = 2.0f;
		fadeEffect.fadeOutTime = 1.0f;
		fadeEffect.skippable = true;
		fadeEffect.skippableWhileFadingIn = true;
		
		fadeEffect.textureName = "teamLogo";		
		teamSplashScreen = new SplashScreen(this,fadeEffect);
		fadeEffect.textureName = "engineLogo";
		engineSplashScreen = new SplashScreen(this,fadeEffect);
		
		menuScreen = new MenuScreen(this);
		creditsScreen = new CreditsScreen(this);
		settingsScreen = new SettingsScreen(this);
		instructionScreen = new InstructionScreen(this);
		
		pauseScreen = new PauseScreen(this);
		playScreen = new PlayScreen(this);
		gameOverScreen = new GameOverScreen(this);

	
		buildGameStateGraph();
		setScreen(teamSplashScreen);
	}
	
	public void buildGameStateGraph() {
		teamSplashScreen.nextScreen = engineSplashScreen;
		engineSplashScreen.nextScreen = menuScreen;
		
		
		menuScreen.playScreen = playScreen;
		menuScreen.informationScreen = creditsScreen;
		menuScreen.settingsScreen = settingsScreen;
		menuScreen.instructionScreen = instructionScreen;
		
		creditsScreen.menuScreen = menuScreen;
		settingsScreen.menuScreen = menuScreen;
		instructionScreen.menuScreen = menuScreen;
		
		playScreen.pauseScreen = pauseScreen;
		playScreen.gameOverScreen = gameOverScreen;
		
		pauseScreen.playScreen = playScreen;
		
		gameOverScreen.playScreen = playScreen;
		gameOverScreen.menuScreen = menuScreen;
	}
	
	public void loadHighScore() {
		Json json = new Json();
		FileHandle file = Gdx.files.local(Configuration.getInstance().highscoreSavePath);
	
		if(file.exists()) {
			String jsonData = file.readString();
			jsonData = Base64Coder.decodeString(jsonData);

			try {
				Configuration.getInstance().highscoreDescriptor = json.fromJson(HighScoreDescriptor.class, jsonData);			
			} catch(Exception e ) {
			
				Gdx.app.log("Expendable ","High Score file " + Configuration.getInstance().highscoreSavePath +" loading exception");
				e.printStackTrace();
			}
		}
		else {
			createHighscoreFile();
			loadHighScore();
		}
	}
	
	public void saveHighScore() {
		Json json = new Json();		
		FileHandle file = Gdx.files.local(Configuration.getInstance().highscoreSavePath);

		if(file.exists()) {
			String jsonData = json.toJson(Configuration.getInstance().highscoreDescriptor);
			jsonData = Base64Coder.encodeString(jsonData);
			file.writeString(jsonData, false);
		}
		else {
			createHighscoreFile();
			Gdx.app.log("Expendable ", "File " + Configuration.getInstance().highscoreSavePath + " opening error detected!");
		}
	}
	
	private void createHighscoreFile() {
		
		HighScoreDescriptor desc = new HighScoreDescriptor();
			
		Json json = new Json();		
		FileHandle file = Gdx.files.local(Configuration.getInstance().highscoreSavePath);
		String jsonData = json.toJson(desc);
		
		jsonData = Base64Coder.encodeString(jsonData);
		file.writeString(jsonData, false);
	}
}
